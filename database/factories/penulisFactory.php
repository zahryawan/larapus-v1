<?php

namespace Database\Factories;

use App\Models\penulis;
use Illuminate\Database\Eloquent\Factories\Factory;

class penulisFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = penulis::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
