<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\buku;
use App\Models\petugas;
use App\Models\penulis;
use App\Models\penerbit;
use App\Models\mahasiswas;
use App\Models\penerbits;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Database\QueryException;
// use Auth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends Controller
{
    public function data_mahasiswa(mahasiswas $mahasiswas)
    {
        $dataMahasiswas = $mahasiswas->all();
        return response()->json($dataMahasiswas, Response::HTTP_OK);
    }

    public function updateMahasiswa(Request $request, $id)
    {
        $Mahasiswa = mahasiswas::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'id_mahasiswa'=>'required',
            'nim' =>'required',
            'nama' =>'required',
            'no_telp' =>'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8'
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(), 
                Response::HTTP_UNPROCESSABLE_ENTITY);
            }
    
            try{
                $Mahasiswa->update($request->all());
                $response = [
                    'message' => 'mahasiswa updated',
                    'data' => $Mahasiswa
                ];
                return response()->json($response, Response::HTTP_OK);
    
            }catch(QueryException $e){
                return response()->json([
                    'message' => "Failed" . $e->errorInfo
                ]);
            } 
    }

    public function getMhsById($id)
    {
        $Mahasiswa = mahasiswas::findOrFail($id);
        $response = [
            'message' => 'Detail of Mahasiswa',
            'data' => $Mahasiswa
        ];

        return response()->json($response, Response::HTTP_OK);
    }

    public function deleteMhs($id)
    {
        $Mahasiswa = mahasiswas::findOrFail($id);

        try{
            $Mahasiswa->delete();
            $response = [
                'message' => 'Mahasiswa deleted'
            ];
            return response()->json($response, Response::HTTP_OK);

        }catch(QueryException $e){
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        } 
    }

    public function addMahasiswa(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_mahasiswa'=>'required',
            'nim' =>'required',
            'nama' =>'required',
            'no_telp' =>'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8'
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(), 
                Response::HTTP_UNPROCESSABLE_ENTITY);
            }
    
            try{
                $Mahasiswa = mahasiswas::create($request->all());
                $response = [
                    'message' => 'mahasiswa created',
                    'data' => $Mahasiswa
                ];
                return response()->json($response, Response::HTTP_CREATED);
    
            }catch(QueryException $e){
                return response()->json([
                    'message' => "Failed" . $e->errorInfo
                ]);
            }
    }

// --------------------------Batas----------------------------------

    public function data_buku(buku $buku)
    {
        $dataBuku = $buku->all();
        return response()->json($dataBuku, Response::HTTP_OK);
    }

    public function updateBuku(Request $request, $id)
    {
        $buku = buku::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'id_buku' => 'required',
            'judul_buku'=>'required',
            'id_penulis' =>'required',
            'id_penerbit' =>'required',
            'tahun_terbit' =>'required'
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(), 
                Response::HTTP_UNPROCESSABLE_ENTITY);
            }
    
            try{
                $buku->update($request->all());
                $response = [
                    'message' => 'buku updated',
                    'data' => $buku
                ];
                return response()->json($response, Response::HTTP_OK);
    
            }catch(QueryException $e){
                return response()->json([
                    'message' => "Failed" . $e->errorInfo
                ]);
            } 
    }

    public function getBukuById($id)
    {
        $buku = buku::findOrFail($id);
        $response = [
            'message' => 'Detail of buku',
            'data' => $buku
        ];

        return response()->json($response, Response::HTTP_OK);
    }

    public function deleteBuku($id)
    {
        $buku = buku::findOrFail($id);

        try{
            $buku->delete();
            $response = [
                'message' => 'buku deleted'
            ];
            return response()->json($response, Response::HTTP_OK);

        }catch(QueryException $e){
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        } 
    }

    public function addBuku(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_buku' =>'required',
            'judul_buku'=>'required',
            'id_penulis' =>'required',
            'id_penerbit' =>'required',
            'tahun_terbit' =>'required'
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(), 
                Response::HTTP_UNPROCESSABLE_ENTITY);
            }
    
            try{
                $buku = buku::create($request->all());
                $response = [
                    'message' => 'buku created',
                    'data' => $buku
                ];
                return response()->json($response, Response::HTTP_CREATED);
    
            }catch(QueryException $e){
                return response()->json([
                    'message' => "Failed" . $e->errorInfo
                ]);
            }
    }

// --------------------------Batas----------------------------------

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' =>'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
    }

    public  function login(Request $request, User $user)
    {
        if(!Auth::attempt(['email' => $request->email, 'password' => $request->password]))
        {
            return response()->json(['error'=> 'wrong'], 401);
        }
        $user = $user->find(Auth::user()->id);
        return response()->json($user);
    }

// --------------------------Batas----------------------------------

    public function data_penulis(penulis $penulis)
    {
        $datapenulis = $penulis->all();
        $respose=[
            'message' => 'List data penulis',
            'data' => $datapenulis
        ];
        return response()->json($respose, Response::HTTP_OK);
    }

    public function data_penerbit(penerbits $penerbit)
    {
        $datapenerbit = $penerbit->all();
        $respose=[
            'message' => 'List data penerbit',
            'data' => $datapenerbit
        ];
        return response()->json($respose, Response::HTTP_OK);
    }

// --------------------------Batas----------------------------------


    public function data_petugas(petugas $petugas)
    {
        $dataPetugas = $petugas->all();
        $respose=[
            'message' => 'List data petugas',
            'data' => $dataPetugas
        ];
        return response()->json($respose, Response::HTTP_OK);
    }

    public function data_user(User $user)
    {
        $dataUser = $user->all();
        $respose=[
            'message' => 'List data petugas',
            'data' => $dataUser
        ];
        return response()->json($respose, Response::HTTP_OK);
    }

    public function addUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' =>'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 
            Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try{
            $User = User::create($request->all());
            $response = [
                'message' => 'user created',
                'data' => $User
            ];
            return response()->json($response, Response::HTTP_CREATED);

        }catch(QueryException $e){
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        } 
    }


    public function updateUser(Request $request, $id)
    {
        $User = User::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'name' =>'required',
            'email' => 'required|email',
            'password' => 'required|min:8'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 
            Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try{
            $User->update($request->all());
            $response = [
                'message' => 'user updated',
                'data' => $User
            ];
            return response()->json($response, Response::HTTP_OK);

        }catch(QueryException $e){
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        } 
    }

    public function getUserById($id)
    {
        $User = User::findOrFail($id);
        $response = [
            'message' => 'Detail of User',
            'data' => $User
        ];

        return response()->json($response, Response::HTTP_OK);
    }

    public function deleteUser($id)
    {
        $User = User::findOrFail($id);

        try{
            $User->delete();
            $response = [
                'message' => 'user deleted'
            ];
            return response()->json($response, Response::HTTP_OK);

        }catch(QueryException $e){
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        } 
    }
   
}


