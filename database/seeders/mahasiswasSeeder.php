<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class mahasiswasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mahasiswas')->insert([
            'id_mahasiswa' => '5',
            'nim' => '185150401111983',
            'nama' => 'Selly Aqidatul',
            'no_telp' => '081740166193',
            'email' => 'selly@mahasiswa.w',
            'password' => Hash::make('33333333'),
        ]);
        DB::table('mahasiswas')->insert([
            'id_mahasiswa' => '6',
            'nim' => '185150401111933',
            'nama' => 'Griscipta Yosefanita',
            'no_telp' => '081975881048',
            'email' => 'griscipta@mahasiswa.w',
            'password' => Hash::make('33333333'),
        ]);
        DB::table('mahasiswas')->insert([
            'id_mahasiswa' => '7',
            'nim' => '185150401111976',
            'nama' => 'Syahri Awan',
            'no_telp' => '081743975911',
            'email' => 'syahri@mahasiswa.w',
            'password' => Hash::make('33333333'),
        ]);
        DB::table('mahasiswas')->insert([
            'id_mahasiswa' => '8',
            'nim' => '185150401111999',
            'nama' => 'Wildan Cahya',
            'no_telp' => '081627384950',
            'email' => 'wildan@mahasiswa.w',
            'password' => Hash::make('33333333'),
        ]);
        DB::table('mahasiswas')->insert([
            'id_mahasiswa' => '9',
            'nim' => '185150401111952',
            'nama' => 'Kukuh Fajar',
            'no_telp' => '081285773433',
            'email' => 'kukuh@mahasiswa.w',
            'password' => Hash::make('33333333'),
        ]);
    }
}
