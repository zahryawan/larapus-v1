@extends('layout.template')
@section('title','Peminjaman')

@section('content')
    <h1>Daftar Peminjaman</h1>
    <a href="/peminjaman/add" class="btn btn-primary">Add Data</a>
    <br><br>
    @if (session('pesan'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i>Success!</h4> 
            <div>{{session('pesan')}}</div>
        </div>    
    @endif
    <table class="table table-bordered">
        <thead>
            <th>NO</th>
            <th>ID TRANSAKSI</th>
            <th>JUDUL BUKU</th>
            <th>NIM MAHASISWA</th>
            <th>NAMA PETUGAS</th>
            <th>tanggal pinjam</th>
            <th>tanggal kembali</th>
        </thead>
        <?php $no=1;?>
        @foreach($data as $data)
            <tr>
                <td>{{$no++}}</td>
                <td>{{$data->id_transaksi}}</td>
                <td>{{$data->relasiBuku->judul_buku}}</td>
                <td>{{$data->relasiMahasiswas->nim}}</td>
                <td>{{$data->relasiPetugas->nama_petugas}}</td>
                <td>{{$data->pinjam}}</td>
                <td>{{$data->kembali}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
