<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(mahasiswasSeeder::class);
        $this->call(penerbitsSeeder::class);
        $this->call(penulisSeeder::class);
        $this->call(petugasSeeder::class);
        $this->call(bukusSeeder::class);
        $this->call(peminjamansSeeder::class);
        $this->call(userSeeder::class);
    }
}
