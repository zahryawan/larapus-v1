<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class petugas extends Model
{
    use HasFactory;
    public function relasi(){
        return $this->hasMany(peminjamans::class,'id_petugas','id_petugas');
    }

    public function detailData($id_petugas) {
        return DB::table('petugas')->where('id_petugas', $id_petugas)->first();
    }

    public function addData($data)
    {
        DB::table('petugas')->insert($data);
    }

    public function editData($id_petugas,$data)
    {
        DB::table('petugas')->where('id_petugas', $id_petugas)->update($data);
    }

    public function deleteData($id_petugas){
        DB::table('petugas')->where('id_petugas', $id_petugas)->delete();
    }
}
