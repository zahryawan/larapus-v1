<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\adminController;
use App\Http\Controllers\mahasiswasController;
use App\Http\Controllers\peminjamansController;
use App\Http\Controllers\petugasController;
use App\Http\Controllers\penerbitsController;
use App\Http\Controllers\penulisController;
use App\Http\Controllers\bukusController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

route::view('/mahasiswa','mahasiswa');
route::view('/petugas','petugas');
route::view('/peminjaman','peminjaman');
route::view('/buku','buku');
route::view('/login','login');


// Route::get('/peminjaman',[peminjamansController::class,'index'])->name('peminjaman')->middleware(['Admin', 'Petugas']);
// Route::get('/peminjaman/add',[peminjamansController::class,'add'])->middleware(['Admin', 'Petugas']);
// Route::post('/peminjaman/insert',[peminjamansController::class,'insert']);

// Route::get('/buku',[bukusController::class,'index'])->name('buku');
// Route::get('/buku/add',[bukusController::class,'add'])->middleware(['Admin', 'Petugas']);
// Route::post('/buku/insert',[bukusController::class,'insert']);
// Route::get('/buku/delete/{id_buku}',[bukusController::class,'delete'])->middleware(['Admin', 'Petugas']);

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/',[AdminController::class,'index'])->name('index');
Route::get('/home',[HomeController::class,'index'])->name('home');

Route::get('/buku',[bukusController::class,'index'])->name('buku');

////////////
//// ADMIN
Route::group(['middleware' => 'Admin'], function(){

    Route::get('/mahasiswa',[mahasiswasController::class,'index'])->name('mahasiswa');
    Route::get('/mahasiswa/detail/{id_mahasiswa}',[mahasiswasController::class,'detail']);
    Route::get('/mahasiswa/add',[mahasiswasController::class,'add']);
    Route::post('/mahasiswa/insert',[mahasiswasController::class,'insert']);
    Route::get('/mahasiswa/edit/{id_mahasiswa}',[mahasiswasController::class,'edit']);
    Route::post('/mahasiswa/update/{id_mahasiswa}',[mahasiswasController::class,'update']);
    Route::get('/mahasiswa/delete/{id_mahasiswa}',[mahasiswasController::class,'delete']);
    // Route::delete('/mahasiswa/{id_mahasiswa}',[mahasiswasController::class,'delete']);

    Route::get('/petugas',[petugasController::class,'index'])->name('petugas');
    Route::get('/petugas/detail/{id_petugas}',[petugasController::class,'detail']);
    Route::get('/petugas/add',[petugasController::class,'add']);
    Route::post('/petugas/insert',[petugasController::class,'insert']);
    Route::get('/petugas/edit/{id_petugas}',[petugasController::class,'edit']);
    Route::post('/petugas/update/{id_petugas}',[petugasController::class,'update']);
    Route::get('/petugas/delete/{id_petugas}',[petugasController::class,'delete']);
    // Route::delete('/petugas/{id_petugas}',[petugasController::class,'delete']);

    Route::get('/penerbit',[penerbitsController::class,'index'])->name('penerbit');

    Route::get('/penulis',[penulisController::class,'index'])->name('penulis');
});

////////////
//// PETUGAS
Route::group(['middleware' => 'Petugas'], function () {
    Route::get('/peminjaman',[peminjamansController::class,'index'])->name('peminjaman');
    Route::get('/peminjaman/add',[peminjamansController::class,'add']);
    Route::post('/peminjaman/insert',[peminjamansController::class,'insert']);

    Route::get('/buku/add',[bukusController::class,'add']);
    Route::post('/buku/insert',[bukusController::class,'insert']);
    Route::get('/buku/delete/{id_buku}',[bukusController::class,'delete']);
});