<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class buku extends Model
{
    protected $fillable = [
        "id_buku",
        "judul_buku",
        "id_penulis",
        "id_penerbit",
        "tahun_terbit"
    ];
    use HasFactory;
    public function relasiPenerbits(){
        return $this->belongsTo(penerbits::class,'id_penerbit','id_penerbit');
    }
    public function relasiPenulis(){
        return $this->belongsTo(penulis::class,'id_penulis','id_penulis');
    }
    public function relasi(){
        return $this->hasMany(peminjamans::class,'id_buku','id_buku');
    }

    public function addData($data)
    {
        DB::table('bukus')->insert($data);
    }

    public function deleteData($id)
    {
        DB::table('bukus')->where('id_buku', $id)->delete();
    }
}
