<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class petugasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('petugas')->insert([
            'id_petugas' => '111',
            'nip' => '786418761',
            'nama_petugas' => 'Montu',
            'jadwal' => 'Kamis',
            'no_telp' => '08511895771',
            'email' => 'montu@petugas.w',
            'password' => Hash::make('22222222'),
        ]);
        DB::table('petugas')->insert([
            'id_petugas' => '112',
            'nip' => '716407153',
            'nama_petugas' => 'Balfir',
            'jadwal' => 'Selasa',
            'no_telp' => '089614401215',
            'email' => 'balfir@petugas.w',
            'password' => Hash::make('22222222'),
        ]);
        DB::table('petugas')->insert([
            'id_petugas' => '138',
            'nip' => '686136922',
            'nama_petugas' => 'Wijaya',
            'jadwal' => 'Selasa',
            'no_telp' => '081650190124',
            'email' => 'wijaya@petugas.w',
            'password' => Hash::make('22222222'),
        ]);
    }
}
