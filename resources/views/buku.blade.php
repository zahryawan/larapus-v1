@extends('layout.template')
@section('title','BUKU')


@section('content')
    <h1>Daftar Buku</h1>
    
    @if (Auth::user()->level == 2)
    <a href="/buku/add" class="btn btn-primary">Add Data</a>
    @endif
    <br><br>
    @if (session('pesan'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i>Success!</h4> 
            <div>{{session('pesan')}}</div>
        </div>    
    @endif
    <table class="table table-bordered">
        <thead>
            <th>NO</th>
            <th>ID</th>
            <th>JUDUL</th>
            <th>PENULIS</th>
            <th>PENERBIT</th>
            <th>TAHUN TERBIT</th>
            @if (Auth::user()->level == 1 || Auth::user()->level == 2)
            <th>ACTION</th>
            @endif
        </thead>
        <?php $no=1;?>
        @foreach($data as $data)
            <tr>
                <td>{{$no++}}</td>
                <td>{{$data->id_buku}}</td>
                <td>{{$data->judul_buku}}</td>
                <td>{{$data->relasiPenulis->nama_penulis}}</td>
                <td>{{$data->relasiPenerbits->nama_penerbit}}</td>
                <td>{{$data->tahun_terbit}}</td>
                @if (Auth::user()->level == 1 || Auth::user()->level == 2)
                <td>
                    <button type="button" class="btn btn-danger" onclick="return confirm('Yakin hapus data?')">
                        <a href="/buku/delete/{{$data->id_buku}}" style="color:white">Delete</a>
                    </button>
                </td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
    <br>

@endsection
