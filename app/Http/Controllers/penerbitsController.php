<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\penerbits;

class penerbitsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index(){
        $data = penerbits::all();
        return view('penerbit',compact('data'));
    }
}
