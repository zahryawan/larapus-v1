<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBukusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bukus', function (Blueprint $table) {
            $table->id();
            $table->integer('id_buku')->unique()->unsigned();
            $table->string('judul_buku');
            $table->integer('id_penulis')->unsigned();;
            $table->foreign('id_penulis')->references('id_penulis')->on('penulis')->onDelete('cascade');
            $table->integer("id_penerbit")->unsigned();;
            $table->foreign('id_penerbit')->references('id_penerbit')->on('penerbits')->onDelete('cascade');
            $table->integer('tahun_terbit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bukus');
    }
}
