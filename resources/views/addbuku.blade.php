@extends('layout.template')
@section('title','Add Petugas')

@section('content')

    <form action="/buku/insert" method="POST">
        @csrf
        <div class="col-sm-6">
            <div class="form-group">
                <label >ID Buku</label>
                <input class="form-control" name="id_buku" value="{{old('id_buku')}}">
                <div class="text-danger">
                    @error('id_buku')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >Judul Buku</label>
                <input class="form-control" name="judul_buku" value="{{old('judul_buku')}}">
                <div class="text-danger">
                    @error('judul_buku')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >ID / Nama Penulis</label><br>
                <select class="form-select" name="id_penulis">
                    <option selected>Select from this menu</option>
                @foreach($penulis as $data)
                    <option value="{{$data->id_penulis}}">{{$data->id_penulis}} - {{$data->nama_penulis}}</option>
                @endforeach
                </select>
                <div class="text-danger">
                    @error('id_penulis')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >ID / Nama Penerbit</label><br>
                <select class="form-select" name="id_penerbit">
                    <option selected>Select from this menu</option>
                @foreach($penerbits as $data)
                    <option value="{{$data->id_penerbit}}">{{$data->id_penerbit}} - {{$data->nama_penerbit}}</option>
                @endforeach
                </select>
                <div class="text-danger">
                    @error('id_penerbit')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >Tahun Terbit</label>
                <input class="form-control" name="tahun_terbit" value="{{old('tahun_terbit')}}">
                <div class="text-danger">
                    @error('tahun_terbit')
                        {{ $message }}
                    @enderror
                </div>
            </div><br>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection
