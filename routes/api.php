<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::get('data_mahasiswa', [ApiController::class, 'data_mahasiswa']);
Route::put('update_mahasiswa/{id}', [ApiController::class, 'updateMahasiswa']);
Route::get('get_mahasiswa/{id}', [ApiController::class, 'getMhsById']);
Route::delete('delete_mahasiswa/{id}', [ApiController::class, 'deleteMhs']);
Route::post('add_mahasiswa', [ApiController::class, 'addMahasiswa']);

// ------------------

Route::get('data_buku', [ApiController::class, 'data_buku']);
Route::put('update_buku/{id}', [ApiController::class, 'updateBuku']);
Route::get('get_buku/{id}', [ApiController::class, 'getBukuById']);
Route::delete('delete_buku/{id}', [ApiController::class, 'deleteBuku']);
Route::post('add_buku', [ApiController::class, 'addBuku']);

// ------------------

Route::get('data_penerbit', [ApiController::class, 'data_penerbit']);
Route::get('data_penulis', [ApiController::class, 'data_penulis']);

// ------------------
Route::get('data_user', [ApiController::class, 'data_user']);
Route::post('register', [ApiController::class, 'register']);
Route::post('login', [ApiController::class, 'login']);
Route::post('addUser', [ApiController::class, 'addUser']);
Route::put('update_users/{id}', [ApiController::class, 'updateUser']);
Route::get('get_users/{id}', [ApiController::class, 'getUserById']);
Route::delete('delete_user/{id}', [ApiController::class, 'deleteUser']);
Route::get('data_petugas', [ApiController::class, 'data_petugas']);