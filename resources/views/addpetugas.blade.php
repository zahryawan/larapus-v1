@extends('layout.template')
@section('title','Add Petugas')

@section('content')

    <form action="/petugas/insert" method="POST">
        @csrf
        <div class="col-sm-6">
        <div class="form-group">
                <label >ID Petugas</label>
                <input class="form-control" name="id_petugas" value="{{old('id_petugas')}}">
                <div class="text-danger">
                    @error('id_petugas')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >NIP</label>
                <input class="form-control" name="nip" value="{{old('nip')}}">
                <div class="text-danger">
                    @error('nip')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >Nama Petugas</label>
                <input class="form-control" name="nama_petugas" value="{{old('nama_petugas')}}">
                <div class="text-danger">
                    @error('nama_petugas')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >Jadwal</label>
                <input class="form-control" name="jadwal" value="{{old('jadwal')}}">
                <div class="text-danger">
                    @error('jadwal')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >No. Telp</label>
                <input class="form-control" name="no_telp" value="{{old('no_telp')}}">
                <div class="text-danger">
                    @error('no_telp')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >Email</label>
                <input class="form-control" name="email" value="{{old('email')}}">
                <div class="text-danger">
                    @error('email')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >Password</label>
                <input class="form-control" type="password" name="password" value="{{old('password')}}">
                <div class="text-danger">
                    @error('password')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection
