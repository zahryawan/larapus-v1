<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\peminjamans;
use App\Models\buku;
use App\Models\mahasiswas;
use App\Models\petugas;

class peminjamansController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->peminjamans = new peminjamans();
    }

    public function index(){
        $data = peminjamans::all();
        return view('peminjaman',compact('data'));
    }

    public function add(){
        $mhs = mahasiswas::all();
        $buku = buku::all();
        $ptgs = petugas::all();
        return view('addpeminjaman',compact('mhs','buku','ptgs'));
    }

    public function insert(){
        Request()->validate([
            'id_transaksi' => 'required|unique:peminjamans,id_transaksi|min:1|max:10',
            'id_buku' => 'required',
            'id_mahasiswa' => 'required',
            'id_petugas' => 'required',
            'pinjam' => 'required',
            'kembali' => 'required',
        ]);
        $data = [
            'id_transaksi'=>Request()->id_transaksi,
            'id_buku' => Request()->id_buku,
            'id_mahasiswa' => Request()->id_mahasiswa,
            'id_petugas' => Request()->id_petugas,
            'pinjam' => Request()->pinjam,
            'kembali'=> Request()->kembali,
        ];
        $this->peminjamans->addData($data);
        return redirect()->route('peminjaman')->with('pesan', 'Data berhasil ditambahkan!');
    }
}
