<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\admin;
use App\Models\buku;
use App\Models\mahasiswas;
use App\Models\petugas;

class adminController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $data0 = buku::all();
        $data1 = mahasiswas::all();
        $data2 = petugas::all();
        return view('admin.read',compact('data1','data2','data0'));
    }
}
