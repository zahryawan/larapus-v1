@extends('layout.template')
@section('title','Add Mahasiswa')

@section('content')

    <form action="/mahasiswa/insert" method="POST">
        @csrf
        <div class="col-sm-6">
        <div class="form-group">
                <label >ID Mahasiswa</label>
                <input class="form-control" name="id_mahasiswa" value="{{old('id_mahasiswa')}}">
                <div class="text-danger">
                    @error('id_mahasiswa')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >NIM</label>
                <input class="form-control" name="nim" value="{{old('nim')}}">
                <div class="text-danger">
                    @error('nim')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >Nama</label>
                <input class="form-control" name="nama" value="{{old('nama')}}">
                <div class="text-danger">
                    @error('nama')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >No. Telp</label>
                <input class="form-control" name="no_telp" value="{{old('no_telp')}}">
                <div class="text-danger">
                    @error('no_telp')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >Email</label>
                <input class="form-control" name="email" value="{{old('email')}}">
                <div class="text-danger">
                    @error('email')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >Password</label>
                <input class="form-control" type="password" name="password" value="{{old('password')}}">
                <div class="text-danger">
                    @error('password')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection
