<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class penulisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('penulis')->insert([
            'id_penulis' => '1234567',
            'nama_penulis' => 'Sari Bunga Mawar',
        ]);
        DB::table('penulis')->insert([
            'id_penulis' => '7654321',
            'nama_penulis' => 'Sulistiani Rozak',
        ]);

        $faker = Faker::create();
        foreach (range(1,98) as $value) {
            DB::table('penulis')->insert([
                'id_penulis' => $faker->numerify('#######'),
                'nama_penulis' => $faker->name,
            ]);
        }
    }
}
