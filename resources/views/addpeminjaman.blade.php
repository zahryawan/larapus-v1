@extends('layout.template')
@section('title','Add Petugas')

@section('content')

    <form action="/peminjaman/insert" method="POST">
        @csrf
        <div class="col-sm-6">
        <div class="form-group">
                <label >ID Transaksi</label>
                <input class="form-control" name="id_transaksi" value="{{old('id_transaksi')}}">
                <div class="text-danger">
                    @error('id_transaksi')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >Judul Buku</label><br>
                <select class="form-select" name="id_buku">
                    <option selected>Select from this menu</option>
                @foreach($buku as $data)
                    <option value="{{$data->id_buku}}">{{$data->judul_buku}}</option>
                @endforeach
                </select>
                <div class="text-danger">
                    @error('id_buku')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >Nama Mahasiswa</label><br>
                <select class="form-select" name="id_mahasiswa">
                    <option selected>Select from this menu</option>
                @foreach($mhs as $data)
                    <option value="{{$data->id_mahasiswa}}">{{$data->nim}} - {{$data->nama}}</option>
                @endforeach
                </select>
                <div class="text-danger">
                    @error('id_mahasiswa')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >Nama Pegawai</label><br>
                <select class="form-select" name="id_petugas">
                    <option selected>Select from this menu</option>
                @foreach($ptgs as $data)
                    <option value="{{$data->id_petugas}}">{{$data->nip}} - {{$data->nama_petugas}}</option>
                @endforeach
                </select>
                <div class="text-danger">
                    @error('id_petugas')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >Tanggal Pinjam</label>
                <input class="form-control" type="date" name="pinjam" value="{{old('pinjam')}}">
                <div class="text-danger">
                    @error('pinjam')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >Tanggal Kembali</label>
                <input class="form-control" type="date" name="kembali" value="{{old('kembali')}}">
                <div class="text-danger">
                    @error('kembali')
                        {{ $message }}
                    @enderror
                </div>
            </div><br>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection
