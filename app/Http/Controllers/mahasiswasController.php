<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\mahasiswas;
use Illuminate\Support\Facades\Hash;

class mahasiswasController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->mahasiswas = new mahasiswas();
    }

    public function index(){
        $data = mahasiswas::all();
        return view('mahasiswa',compact('data'));
    }

    public function detail($id_mahasiswa){
        if (!$this->mahasiswas->detailData($id_mahasiswa)){
            abort(404);
        }
        $data = [
            'mahasiswa' => $this->mahasiswas->detailData($id_mahasiswa),
        ];
        return view('detailmahasiswa', $data);
    }

    public function add(){
        return view('addmahasiswa');
    }

    public function insert(){
        Request()->validate([
            'id_mahasiswa' => 'required|unique:mahasiswas,id_mahasiswa|min:2|max:10',
            'nim' => 'required|unique:mahasiswas,nim|min:9|max:255',
            'nama' => 'required',
            'no_telp' => 'required',
            'email' => 'required',
            'password' => 'required|min:8',
        ]);
        $data1 = [
            'id_mahasiswa'=>Request()->id_mahasiswa,
            'nim' => Request()->nim,
            'nama' => Request()->nama,
            'no_telp' => Request()->no_telp,
            'email' => Request()->email,
            'password'=>Hash::make(Request()->password),
        ];
        $data2 = [
            'name' => Request()->nama,
            'email' => Request()->email,
            'password'=> Hash::make(Request()->password),
        ];
        $this->mahasiswas->addData($data1,$data2);
        return redirect()->route('mahasiswa')->with('pesan', 'Data berhasil ditambahkan!');
    }

    public function edit($id_mahasiswa){
        if (!$this->mahasiswas->detailData($id_mahasiswa)){
            abort(404);
        }
        $data = [
            'mahasiswa' => $this->mahasiswas->detailData($id_mahasiswa),
        ];
        return view('editmahasiswa',$data);
    }

    public function update($id_mahasiswa){
        Request()->validate([
            'id_mahasiswa' => 'required|min:2|max:10',
            'nim' => 'required|min:9|max:255',
            'nama' => 'required',
            'no_telp' => 'required',
            'email' => 'required',
            'password' => 'required|min:8',
        ]);
        $data = [
            'id_mahasiswa'=>Request()->id_mahasiswa,
            'nim' => Request()->nim,
            'nama' => Request()->nama,
            'no_telp' => Request()->no_telp,
            'email' => Request()->email,
            'password'=>Hash::make(Request()->password),
        ];
        $this->mahasiswas->editData($id_mahasiswa,$data);
        return redirect()->route('mahasiswa')->with('pesan', 'Data berhasil diupdate!');
    }
    
    public function delete($id_mahasiswa){
        $this->mahasiswas->deleteData($id_mahasiswa);
        return redirect()->route('mahasiswa')->with('pesan', 'Data berhasil dihapus!');
    }



}