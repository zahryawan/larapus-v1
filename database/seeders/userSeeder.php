<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class userSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Wildan',
            'email' => 'wildan@mahasiswa.w',
            'password' => Hash::make('11111111'),
            'level' => '1',
        ]);



        DB::table('users')->insert([
            'name' => 'Montu',
            'email' => 'montu@petugas.w',
            'password' => Hash::make('22222222'),
            'level' => '2',
        ]);
        DB::table('users')->insert([
            'name' => 'Balfir',
            'email' => 'balfir@petugas.w',
            'password' => Hash::make('22222222'),
            'level' => '2',
        ]);
        DB::table('users')->insert([
            'name' => 'Wijaya',
            'email' => 'wijaya@petugas.w',
            'password' => Hash::make('22222222'),
            'level' => '2',
        ]);



        DB::table('users')->insert([
            'name' => 'Selly Aqidatul',
            'email' => 'selly@mahasiswa.w',
            'password' => Hash::make('33333333'),
        ]);
        DB::table('users')->insert([
            'name' => 'Griscipta Yosefanita',
            'email' => 'griscipta@mahasiswa.w',
            'password' => Hash::make('33333333'),
        ]);
        DB::table('users')->insert([
            'name' => 'Syahri Awan',
            'email' => 'syahri@mahasiswa.w',
            'password' => Hash::make('33333333'),
        ]);
        DB::table('users')->insert([
            'name' => 'Kukuh Fajar',
            'email' => 'kukuh@mahasiswa.w',
            'password' => Hash::make('33333333'),
        ]);
    }
}
