@extends('layout.template')
@section('title','Mahasiswa')


@section('content')
    <h1>Daftar Mahasiswa</h1>
    <a href="/mahasiswa/add" class="btn btn-primary">Add Data</a>
    <br><br>
    @if (session('pesan'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i>Success!</h4> 
            <div>{{session('pesan')}}</div>
        </div>    
    @endif


    <table class="table table-bordered">
        <thead>
            <th>NO</th>
            <th>NIM</th>
            <th>NAMA</th>
            <th>NO. HP</th>
            <th>EMAIL</th>
            <th>ACTIONS</th>
        </thead>
        <tbody>
        <?php $no=1;?>
        @foreach($data as $data)
            <tr>
                <td>{{$no++}}</td>
                <td>{{$data->nim}}</td>
                <td>{{$data->nama}}</td>
                <td>{{$data->no_telp}}</td>
                <td>{{$data->email}}</td>
                <td>
                    <a href="/mahasiswa/detail/{{$data->id_mahasiswa}}" class="btn btn-sn btn-success">Detail</a>
                    <a href="/mahasiswa/edit/{{$data->id_mahasiswa}}" class="btn btn-sn btn-warning">Edit</a>
                    <!-- <form action="{{ url('mahasiswa/'.$data->id_mahasiswa) }}" method="POST" class="d-inline" onsubmit="return confirm('Yakin hapus data?')"> 
                    @method('delete')
                    @csrf
                    <button class="btn btn-danger" type="submit">Delete</button>
                    </form> -->
                    <button type="button" class="btn btn-danger" onclick="return confirm('Yakin hapus data?')">
                        <a href="/mahasiswa/delete/{{$data->id_mahasiswa}}" style="color:white">Delete</a>
                    </button>
                    <!-- <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete{{$data->id_mahasiswa}}">
                        Delete
                    </button> -->
                    <!-- <a href="#modalDelete" data-toggle="modal" onclick="$('#modalDelete #formDelete').attr('action', '{{$data->id_mahasiswa}}'))" class="btn btn-sn btn-danger">Delete</a> -->
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
            <!-- 
            <div class="modal fade" id="modalDelete">
                <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <p>Apakah Anda yakin ingin menghapus data</p>
                    </div>
                    <div class="modal-footer">
                        <form id="formDelete" action="" method="POST">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
                        <button type="Submit" class="btn btn-danger pull-right" >Hapus</button>
                        </form>
                    </div>
                </div>
                </div>
            </div> -->
    @endsection