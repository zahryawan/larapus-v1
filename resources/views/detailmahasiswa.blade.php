@extends('layout.template')
@section('title','Detail Mahasiswa')

@section('content')

<table class="table">
    <tr>
        <th width="100px">NIM</th>
        <th width="30px">:</th>
        <th>{{$mahasiswa->nim}}</th>
    </tr>
    <tr>
        <th width="100px">NAMA</th>
        <th width="30px">:</th>
        <th>{{$mahasiswa->nama}}</th>
    </tr>
    <tr>
        <th width="100px">NO. HP</th>
        <th width="30px">:</th>
        <th>{{$mahasiswa->no_telp}}</th>
    </tr>
    <tr>
        <th width="100px">EMAIL</th>
        <th width="30px">:</th>
        <th>{{$mahasiswa->email}}</th>
    </tr>
    <tr>
        <th>
        <a href="/mahasiswa/" class="btn btn-sn btn-success">Kembali</a>
        </th>
    </tr>
</table>








@endsection
