@extends('layout.template')
@section('title','Edit Mahasiswa')

@section('content')

    <form action="/mahasiswa/update/{{$mahasiswa->id_mahasiswa}}" method="POST">
        @csrf
        <div class="col-sm-6">
        <div class="form-group">
                <label >ID Mahasiswa</label>
                <input class="form-control" name="id_mahasiswa" value="{{$mahasiswa->id_mahasiswa}}" readonly>
                <div class="text-danger">
                    @error('id_mahasiswa')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >NIM</label>
                <input class="form-control" name="nim" value="{{$mahasiswa->nim}}"readonly>
                <div class="text-danger">
                    @error('nim')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >Nama</label>
                <input class="form-control" name="nama" value="{{$mahasiswa->nama}}">
                <div class="text-danger">
                    @error('nama')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >No. Telp</label>
                <input class="form-control" name="no_telp" value="{{$mahasiswa->no_telp}}">
                <div class="text-danger">
                    @error('no_telp')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >Email</label>
                <input class="form-control" name="email" value="{{$mahasiswa->email}}">
                <div class="text-danger">
                    @error('email')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label >Password</label>
                <input class="form-control" type="password" name="password" value="{{$mahasiswa->password}}">
                <div class="text-danger">
                    @error('password')
                        {{ $message }}
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </div>
    </form>
@endsection
