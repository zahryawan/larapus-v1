<ul class="sidebar-menu" data-widget="tree">
    <li class="header" style="color:white">MAIN NAVIGATION</li>
    <li class="{{ request()->is('/') ? 'active' : '' }}{{ request()->is('home') ? 'active' : '' }}"><a href="/"><i class="fa fa-home"></i> <span>Home</span></a></li>

    @if(Auth::user()->level == 1)
    <li class="{{ request()->is('mahasiswa') ? 'active' : '' }}"><a href="/mahasiswa"><i class="fa fa-graduation-cap"></i> <span>Mahasiswa</span></a></li>
    <li class="{{ request()->is('petugas') ? 'active' : '' }}"><a href="/petugas"><i class="fa fa-user"></i> <span>Petugas</span></a></li>
    <li class="{{ request()->is('penerbit') ? 'active' : '' }}"><a href="/penerbit"><i class="fa fa-users"></i> <span>Penerbit</span></a></li>
    <li class="{{ request()->is('penulis') ? 'active' : '' }}"><a href="/penulis"><i class="fa fa-pencil"></i> <span>Penulis</span></a></li>
    
    @elseif(Auth::user()->level == 2)
    <li class="{{ request()->is('peminjaman') ? 'active' : '' }}"><a href="/peminjaman"><i class="fa fa-link"></i> <span>Peminjaman</span></a></li>
    <li class="{{ request()->is('buku') ? 'active' : '' }}"><a href="/buku"><i class="fa fa-book"></i> <span>Buku</span></a></li>
    
    @else
    <li class="{{ request()->is('buku') ? 'active' : '' }}"><a href="/buku"><i class="fa fa-book"></i> <span>Buku</span></a></li>
    
    @endif
    <!-- <li class="treeview">
      <a href="#">
        <i class="fa fa-share"></i> <span>Multilevel</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
        <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
      </ul>
    </li> -->

  </ul>
