<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class bukusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bukus')->insert([
            'id_buku' => '9090',
            'judul_buku' => 'Majalah Anak-anak',
            'id_penulis' => '1234567',
            'id_penerbit' => '54321',
            'tahun_terbit' => '2003',
        ]);
        DB::table('bukus')->insert([
            'id_buku' => '9999',
            'judul_buku' => 'Majalah Dewasa',
            'id_penulis' => '7654321',
            'id_penerbit' => '12345',
            'tahun_terbit' => '2020',
        ]);
    }
}
