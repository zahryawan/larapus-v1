<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mahasiswas extends Model
{protected $fillable = [
    'id_mahasiswa',
    'nim',
    'nama',
    'no_telp',
    'email',
    'password',
];
    use HasFactory;
    public function relasi(){
        return $this->hasMany(peminjamans::class,'id_mahasiswa','id_mahasiswa');
    }

    public function detailData($id_mahasiswa) {
        return DB::table('mahasiswas')->where('id_mahasiswa', $id_mahasiswa)->first();
    }

    public function addData($data1,$data2)
    {
        DB::table('mahasiswas')->insert($data1);
        DB::table('users')->insert($data2);
    }

    public function editData($id_mahasiswa,$data)
    {
        DB::table('mahasiswas')->where('id_mahasiswa', $id_mahasiswa)->update($data);
    }

    public function deleteData($id_mahasiswa)
    {
        DB::table('mahasiswas')->where('id_mahasiswa', $id_mahasiswa)->delete();
    }
}
