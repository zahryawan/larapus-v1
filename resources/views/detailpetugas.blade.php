@extends('layout.template')
@section('title','Detail Petugas')

@section('content')

<table class="table">
    <tr>
        <th width="100px">NIP</th>
        <th width="30px">:</th>
        <th>{{$petugas->nip}}</th>
    </tr>
    <tr>
        <th width="100px">NAMA</th>
        <th width="30px">:</th>
        <th>{{$petugas->nama_petugas}}</th>
    </tr>
    <tr>
        <th width="100px">JADWAL</th>
        <th width="30px">:</th>
        <th>{{$petugas->jadwal}}</th>
    </tr>
    <tr>
        <th width="100px">NO. HP</th>
        <th width="30px">:</th>
        <th>{{$petugas->no_telp}}</th>
    </tr>
    <tr>
        <th width="100px">EMAIL</th>
        <th width="30px">:</th>
        <th>{{$petugas->email}}</th>
    </tr>
    <tr>
        <th>
        <a href="/petugas/" class="btn btn-sn btn-success">Kembali</a>
        </th>
    </tr>
</table>

@endsection
