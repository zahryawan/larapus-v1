<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class penulis extends Model
{
    use HasFactory;
    public function relasi(){
        return $this->hasMany(buku::class,'id_penulis','id_penulis');
    }
}
