@extends('layout.template')
@section('title','PETUGAS')

@section('content')
    <h1>Daftar Petugas </h1><a href="/petugas/add" class="btn btn-primary">Add Data</a>
    <br><br>
    @if (session('pesan'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i>Success!</h4> 
            <div>{{session('pesan')}}</div>
        </div>    
    @endif
    <table class="table table-bordered">
        <thead>
            <th>NO</th>
            <th>ID</th>
            <th>NIP</th>
            <th>NAMA</th>
            <th>JADWAL</th>
            <th>NO. HP</th>
            <th>EMAIL</th>
            <th>ACTIONS</th>
        </thead>
        <?php $no=1;?>
        @foreach($data as $data)
            <tr>
                <td>{{$no++}}</td>
                <td>{{$data->id_petugas}}</td>
                <td>{{$data->nip}}</td>
                <td>{{$data->nama_petugas}}</td>
                <td>{{$data->jadwal}}</td>
                <td>{{$data->no_telp}}</td>
                <td>{{$data->email}}</td>
                <td>
                    <a href="/petugas/detail/{{$data->id_petugas}}" class="btn btn-sn btn-success">Detail</a>
                    <a href="/petugas/edit/{{$data->id_petugas}}" class="btn btn-sn btn-warning">Edit</a>
                    <!-- <form action="{{ url('petugas/'.$data->id_petugas) }}" method="POST" class="d-inline" onsubmit="return confirm('Yakin hapus data?')"> 
                    @method('delete')
                    @csrf
                    <a><button class="btn btn-sn btn-danger" type="submit">Delete</button></a>
                    </form> -->
                    <button type="button" class="btn btn-danger" onclick="return confirm('Yakin hapus data?')">
                        <a href="/petugas/delete/{{$data->id_petugas}}" style="color:white">Delete</a>
                    </button>
                    <!-- <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete{{$data->id_petugas}}" onclick="return confirm('Yakin hapus data?')">
                        Delete
                    </button> -->
                    
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
