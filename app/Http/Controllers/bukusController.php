<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\buku;
use App\Models\penulis;
use App\Models\penerbits;

class bukusController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->buku = new buku();
    }

    public function index(){
        $data = buku::all();
        return view('buku',compact('data'));
    }

    public function add(){
        $penulis = penulis::all();
        $penerbits = penerbits::all();
        return view('addbuku',compact('penerbits','penulis'));
    }

    public function insert(){
        Request()->validate([
            'id_buku' => 'required|unique:bukus,id_buku|min:1|max:10',
            'judul_buku' => 'required',
            'id_penulis' => 'required',
            'id_penerbit' => 'required',
            'tahun_terbit' => 'required|min:4|max:4',
        ]);
        $data = [
            'id_buku'=>Request()->id_buku,
            'judul_buku' => Request()->judul_buku,
            'id_penulis' => Request()->id_penulis,
            'id_penerbit' => Request()->id_penerbit,
            'tahun_terbit' => Request()->tahun_terbit,
        ];
        $this->buku->addData($data);
        return redirect()->route('buku')->with('pesan', 'Data berhasil ditambahkan!');
    }
    
    public function delete($id){
        $this->buku->deleteData($id);
        return redirect()->route('buku')->with('pesan', 'Data berhasil dihapus!');
    }
}
