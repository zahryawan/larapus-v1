<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class peminjamansSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('peminjamans')->insert([
            'id_transaksi' => '1912700148',
            'id_buku' => '9999',
            'id_mahasiswa' => '5',
            'id_petugas' => '138',
            'pinjam' => '04-10-2020',
            'kembali' => '04-10-2021',
        ]);
        DB::table('peminjamans')->insert([
            'id_transaksi' => '1917033079',
            'id_buku' => '9090',
            'id_mahasiswa' => '6',
            'id_petugas' => '111',
            'pinjam' => '29-01-2021',
            'kembali' => '28-02-2021',
        ]);
    }
}
