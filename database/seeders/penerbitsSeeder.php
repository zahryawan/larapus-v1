<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class penerbitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('penerbits')->insert([
            'id_penerbit' => '12345',
            'nama_penerbit' => 'Kang Alberto',
            'alamat' => '12345 Omahe Kang Alberto, KA 12345',
        ]);
        DB::table('penerbits')->insert([
            'id_penerbit' => '54321',
            'nama_penerbit' => 'Mister Soebardjo',
            'alamat' => '54321 Omahe Mister Soebardjo, MS 54321',
        ]);

        $faker = Faker::create();
        foreach (range(1,48) as $value) {
            DB::table('penerbits')->insert([
                'id_penerbit' => $faker->numerify('#####'),
                'nama_penerbit' => $faker->company,
                'alamat' => $faker->address,
            ]);
        }
    }
}
