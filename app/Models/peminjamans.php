<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class peminjamans extends Model
{
    use HasFactory;
    public function relasiMahasiswas(){
        return $this->belongsTo(mahasiswas::class,'id_mahasiswa','id_mahasiswa');
    }
    public function relasiPetugas(){
        return $this->belongsTo(petugas::class,'id_petugas','id_petugas');
    }
    public function relasiBuku(){
        return $this->belongsTo(buku::class,'id_buku','id_buku');
    }

    public function addData($data)
    {
        DB::table('peminjamans')->insert($data);
    }
}
