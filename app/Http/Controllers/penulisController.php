<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\penulis;

class penulisController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index(){
        $data = penulis::all();
        return view('penulis',compact('data'));
    }
}
