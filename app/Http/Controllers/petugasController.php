<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\petugas;
use Illuminate\Support\Facades\Hash;

class petugasController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->petugas = new petugas();
    }
    
    public function index(){
        $data = petugas::all();
        return view('petugas',compact('data'));
    }

    public function detail($id_petugas){
        if (!$this->petugas->detailData($id_petugas)){
            abort(404);
        }
        $data = [
            'petugas' => $this->petugas->detailData($id_petugas),
        ];
        return view('detailpetugas', $data);
    }

    public function add(){
        return view('addpetugas');
    }

    public function insert(){
        Request()->validate([
            'id_petugas' => 'required|unique:petugas,id_petugas|min:2|max:10',
            'nip' => 'required|unique:petugas,nip|min:9|max:255',
            'nama_petugas' => 'required',
            'jadwal' => 'required',
            'no_telp' => 'required',
            'email' => 'required',
            'password' => 'required|min:8',
        ]);
        $data = [
            'id_petugas'=>Request()->id_petugas,
            'nip' => Request()->nip,
            'nama_petugas' => Request()->nama_petugas,
            'jadwal' => Request()->jadwal,
            'no_telp' => Request()->no_telp,
            'email' => Request()->email,
            'password'=>Hash::make(Request()->password),
        ];
        $this->petugas->addData($data);
        return redirect()->route('petugas')->with('pesan', 'Data berhasil ditambahkan!');
    }

    public function edit($id_petugas){
        if (!$this->petugas->detailData($id_petugas)){
            abort(404);
        }
        $data = [
            'petugas' => $this->petugas->detailData($id_petugas),
        ];
        return view('editpetugas',$data);
    }

    public function update($id_petugas){
        Request()->validate([
            'id_petugas' => 'required|min:2|max:10',
            'nip' => 'required|min:9|max:255',
            'nama_petugas' => 'required',
            'jadwal' => 'required',
            'no_telp' => 'required',
            'email' => 'required',
            'password' => 'required|min:8',
        ]);
        $data = [
            'id_petugas'=>Request()->id_petugas,
            'nip' => Request()->nip,
            'nama_petugas' => Request()->nama_petugas,
            'jadwal' => Request()->jadwal,
            'no_telp' => Request()->no_telp,
            'email' => Request()->email,
            'password'=>Hash::make(Request()->password),
        ];
        $this->petugas->editData($id_petugas,$data);
        return redirect()->route('petugas')->with('pesan', 'Data berhasil diupdate!');
    }
    
    public function delete($id_petugas){
        $this->petugas->deleteData($id_petugas);
        return redirect()->route('petugas')->with('pesan', 'Data berhasil dihapus!');
    }
}
