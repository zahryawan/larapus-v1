@extends('layout.template')
@section('title','PENERBIT')

@section('content')
    <h1>Daftar Penerbit Buku</h1>
    <!-- <a href="/penerbit/add" class="btn btn-primary">Add Data</a> -->
    <a class="btn btn-primary">Add Data</a>
    <br><br>
    @if (session('pesan'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i>Success!</h4> 
            <div>{{session('pesan')}}</div>
        </div>    
    @endif
    <table class="table table-bordered">
        <thead>
            <th>NO</th>
            <th>ID PENERBIT</th>
            <th>NAMA</th>
            <th>ALAMAT</th>
            <th> </th>
            <th> </th>
            <th> </th>
        </thead>
        <?php $no=1;?>
        @foreach($data as $data)
            <tr>
                <td>{{$no++}}</td>
                <td>{{$data->id_penerbit}}</td>
                <td>{{$data->nama_penerbit}}</td>
                <td>{{$data->alamat}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
